# Graphical Human Interface

## Landing Page

This is the first page visitors arrive at when they visit the website. It features a welcoming design with clear navigation options to guide users to the main features of the application, such as creating an event, viewing events, and logging in or signing up.

![Landing Page](wireframes/landing-page.png)

## Signup Page

This page allows new users to create an account. It includes a form where users can enter their email, password, and other necessary information to create an account.

![Signup Page](wireframes/signup-page.png)

## Login Page

This page allows existing users to log in to their account. It includes a form where users can enter their email and password.

![Login Page](wireframes/login-page.png)

## Profile Page

Once logged in, users can visit their profile page to view their account details. This page includes features such as viewing account information, viewing RSVPs, and managing events.

![Profile Page](wireframes/profile-page.png)

## Event Page

This page displays a list of all available events. Each event is displayed as a card with key details. Users can click on an event to view more detailed information, or they can use the integrated Google Map to view the locations of different events.

![Event Page](wireframes/event-page.png)

## Event Details Page

This page displays detailed information about a specific event. It includes all the details entered when the event was created, as well as options to RSVP to the event or view the event on the map.

![Event Details Page](wireframes/event-details-page.png)
