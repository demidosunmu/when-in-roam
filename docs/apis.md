## Log In

- Endpoint path: /token
- Endpoint method: POST

- Request shape (form):

  - username: string
  - password: string

- Response: Token
- Response shape (JSON):

  ```json
  {
    "access_token": "string",
    "token_type": "Bearer"
  }
  ```

---

## Log Out

- Endpoint path: /token
- Endpoint method: DELETE

- Headers:

  - Authorization: Bearer token

- Response: true
- Response shape (JSON):

  ```json
  true
  ```

---

## Accounts

#### **Create an Account**

- Endpoint path: /accounts
- Endpoint method: POST

- Request shape (JSON):

  ```json
  {
    "first_name": "string",
    "last_name": "string",
    "birthday": "string",
    "gender": "string",
    "email": "string",
    "language": ["string"],
    "password": "string"
  }
  ```

---

- Response: Account information and access token
- Response shape (JSON):

  ```json
  {
    "access_token": "string",
    "token_type": "Bearer",
    "account": {
      "id": 0,
      "first_name": "string",
      "last_name": "string",
      "birthday": "string",
      "gender": "string",
      "language": ["string"],
      "email": "string"
    }
  }
  ```

---

#### **Get All Accounts**

- Endpoint path: /accounts/
- Endpoint method: GET

- Response: All existing accounts
- Response shape (JSON):

  ```json
  [
    {
      "id": 0,
      "first_name": "string",
      "last_name": "string",
      "birthday": "string",
      "gender": "string",
      "language": ["string"],
      "email": "string"
    }
  ]
  ```

---

#### **Get Account by Email**

- Endpoint path: /accounts/{email}
- Endpoint method: GET

- Request shape (form):

  - email: string

- Response: Single account with matching email
- Response shape (JSON):

  ```json
  {
    "id": 0,
    "first_name": "string",
    "last_name": "string",
    "birthday": "string",
    "gender": "string",
    "language": ["string"],
    "email": "string"
  }
  ```

---

#### **Get Account by User ID**

- Endpoint path: /accounts/{user_id}/by-id
- Endpoint method: GET

- Request shape (form):

  - user_id: int

- Response: Single account with matching ID
- Response shape (JSON):

  ```json
  {
    "id": 0,
    "first_name": "string",
    "last_name": "string",
    "birthday": "string",
    "gender": "string",
    "language": ["string"],
    "email": "string"
  }
  ```

---

## Events

#### **Create an Event**

- Endpoint path: /events
- Endpoint method: POST

- Headers:

  - Authorization: Bearer token

- Request shape (JSON):

  ```json
  {
    "name": "string",
    "start_date": "string",
    "end_date": "string",
    "address": "string",
    "latitude": 0,
    "longitude": 0,
    "description": "string",
    "minimum_attendees": 0,
    "maximum_attendees": 0,
    "fee": 0,
    "minimum_age": 0,
    "requirements": "string",
    "pet_friendly": false
  }
  ```

- Response: Event information
- Response shape (JSON):

  ```json
  {
    "id": 0,
    "name": "string",
    "start_date": "2023-07-28T20:07:07.042Z",
    "end_date": "2023-07-28T20:07:07.042Z",
    "address": "string",
    "latitude": 0,
    "longitude": 0,
    "description": "string",
    "minimum_attendees": 0,
    "maximum_attendees": 0,
    "fee": 0,
    "minimum_age": 0,
    "requirements": "string",
    "pet_friendly": true
  }
  ```

---

#### **Get All Events**

- Endpoint path: /events/
- Endpoint method: GET

- Response: All existing events
- Response shape (JSON):

  ```json
  [
    {
      "id": 0,
      "name": "string",
      "start_date": "2023-07-28T20:09:19.946Z",
      "end_date": "2023-07-28T20:09:19.946Z",
      "address": "string",
      "latitude": 0,
      "longitude": 0,
      "description": "string",
      "minimum_attendees": 0,
      "maximum_attendees": 0,
      "fee": 0,
      "minimum_age": 0,
      "requirements": "string",
      "pet_friendly": true
    }
  ]
  ```

---

#### **Get Event by Event ID**

- Endpoint path: /events/{event_id}
- Endpoint method: GET

- Request shape (form):

  - event_id: int

- Response: Single account with matching ID
- Response shape (JSON):

  ```json
  {
    "id": 0,
    "name": "string",
    "start_date": "2023-07-28T20:11:18.805Z",
    "end_date": "2023-07-28T20:11:18.805Z",
    "address": "string",
    "latitude": 0,
    "longitude": 0,
    "description": "string",
    "minimum_attendees": 0,
    "maximum_attendees": 0,
    "fee": 0,
    "minimum_age": 0,
    "requirements": "string",
    "pet_friendly": true
  }
  ```

---

#### **Update Event**

- Endpoint path: /events/{event_id}
- Endpoint method: PUT

- Request shape (form, JSON):
  **Only fields to be updated can be included in request JSON**

  - event_id: int

  ```json
  {
    "name": "string",
    "start_date": "string",
    "end_date": "string",
    "address": "string",
    "latitude": 0,
    "longitude": 0,
    "description": "string",
    "minimum_attendees": 0,
    "maximum_attendees": 0,
    "fee": 0,
    "minimum_age": 0,
    "requirements": "string",
    "pet_friendly": true
  }
  ```

- Response: Updated event information
- Response shape (JSON):

  ```json
  {
    "id": 0,
    "name": "string",
    "start_date": "2023-07-28T20:11:18.805Z",
    "end_date": "2023-07-28T20:11:18.805Z",
    "address": "string",
    "latitude": 0,
    "longitude": 0,
    "description": "string",
    "minimum_attendees": 0,
    "maximum_attendees": 0,
    "fee": 0,
    "minimum_age": 0,
    "requirements": "string",
    "pet_friendly": true
  }
  ```

---

#### **Delete Event**

- Endpoint path: /events/{event_id}
- Endpoint method: DELETE

- Request shape (form, JSON):

  - event_id: int

- Response: true
- Response shape (JSON):

  ```json
  true
  ```

---

## RSVPs

#### **Create RSVP for Event**

- Endpoint path: /events/{event_id}/rsvp
- Endpoint method: POST

- Request shape (form):

  - event_id: int

- Response: RSVP successful message
- Response shape (JSON):

  ```json
  {
    "message": "string"
  }
  ```

---

#### **Get RSVPs by Event**

- Endpoint path: /events/{event_id}/rsvp
- Endpoint method: GET

- Request shape (form):

  - event_id: int

- Response: All RSVPs for matching ID
- Response shape (JSON):

  ```json
  [
    {
      "id": 0,
      "event_id": 0,
      "user_id": 0,
      "user_first_name": "string",
      "user_last_name": "string",
      "user_gender": "string"
    }
  ]
  ```

---

#### **Get RSVPs by User**

- Endpoint path: /events/{event_id}/rsvp
- Endpoint method: GET

- Request shape (form):

  - user_id: int

- Response: All RSVPs for matching ID
- Response shape (JSON):

  ```json
  [
    {
      "id": 0,
      "event_id": 0,
      "user_id": 0,
      "event_name": "string",
      "event_start_date": "2023-07-28T20:11:18.805Z",
      "event_end_date": "2023-07-28T20:11:18.805Z",
      "user_first_name": "string",
      "user_last_name": "string",
      "user_gender": "string"
    }
  ]
  ```

---

#### **Cancel RSVP**

- Endpoint path: /events/rsvp/{rsvp_id}
- Endpoint method: DELETE

- Request shape (form, JSON):

  - rsvp_id: int

- Response: true
- Response shape (JSON):

  ```json
  true
  ```

---
