# When in Roam

## Description

When in Roam is a social networking application designed for travelers and expatriates. The platform aims to connect users who find themselves in new places without established social circles, helping them forge meaningful connections and create unforgettable experiences together.

## Key Features

- **Event Creation**: Users can easily create events based on their interests and preferences, such as meetups at local landmarks, group hiking trips, or casual gatherings at popular cafes.

- **RSVP System**: Other users in the same area can RSVP to the events, indicating their interest in participating.

- **Map Feature**: A user-friendly map displays all events in the user's area, making it easy to discover exciting activities nearby and connect with like-minded individuals.

- **Attend and Connect**: Once an event is scheduled and users have RSVPed, they can meet up and build new connections, fostering a sense of community in a foreign land.

## Why When in Roam?

Traveling and living abroad can be exciting, but it can also be challenging to form connections in unfamiliar territory. When in Roam provides a platform where users can break the ice, find like-minded individuals, and create lasting memories in their new surroundings. Whether you're an adventurer exploring distant lands or an expat looking to expand your social circle, When in Roam is here to make your journey more enriching.

## Intended Audience

- **Travelers**: Wanderers who love exploring new destinations and meeting fellow travelers or locals.

- **Expatriates**: Individuals who have relocated to a foreign country and seek to build connections within their adopted community.

- **Social Explorers**: People who enjoy discovering exciting events and activities while meeting diverse groups of individuals.

## Functionality

When In Roam provides a platform for users to create, view, and manage events. Upon visiting the site, users can create a new account or log in to an existing account. Once logged in, users are taken to the map page that displays the locations of all available events. Each event is represented by a marker on the map, which users can click on to get more information about each event. This feature makes it easy for users to find events in their local area or in a specific location they're interested in.

In addition to the map view, users can also toggle to a list view where each event is displayed as a card with an overview of the even'ts details. Users can click on an event card to view more detailed information.

From the map or list view, users can navigate to the event creation form, where they can input all the necessary details for their event.

In addition to managing events, users can also manage their RSVPs for events. They can view all of their upcoming and previous RSVPs on the profile page, create new RSVPs for events, and cancel their RSVPs if they no longer wish to attend an event.

## Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Run docker volume create postgres-data
4. Run docker volume create pg-admin
5. Run docker-compose build
6. Run docker-compose up
7. Enjoy When In Roam to its fullest!

## Future Development

- **Event List Filters**: We'll be adding filters to the event list to make it easier for users to discover events that match their preferences. These filters will allow users to narrow down events within a 50-mile radius, filter by pet-friendly options, preferred languages, and more.
- **Messaging System**: To facilitate communication between event hosts and potential attendees, we plan to implement a messaging system that will enable users to reach out to event hosts
- **Profile Photos and Event Photos**: We'll give users the ability to personalize their profiles with photos, adding a personal touch to their When in Roam presence. Event hosts will be able to upload a photo for the event.
- **Vendors Feature**: Introducing a vendors feature will allow businesses and service providers to create sponsored events, offering unique experiences and opportunities for our community.
- **Calendar View**: We'll implement a calendar view, granting users an overview of all their RSVP'd events. Hosts will have access to a calendar displaying their scheduled events, streamlining event management.
- **Edit and Delete Functionality**: Users will have the ability to edit or delete their own events, providing more control over event details.
- **My Events Section**: A dedicated section on user profiles will showcase all events where the logged-in user is the host, making it convenient to manage events.
- **Profile Editing and Password Reset**: We'll introduce the functionality for users to easily edit their profiles and reset their passwords as needed.
- **Event Pins on the Globe**: Adding event pins to our global map will provide a visual representation of all the exciting activities happening around the world.
- **Event Creation from Profile**: Users will have the option to create new events directly from their profiles, streamlining the event creation process.

As When in Roam evolves, these future developments will bring even more value and enriching experiences to our community of travelers and expatriates.

## Contributors

**Demi Dosunmu** - _Founder, Creator, and API Integration Superstar_<br>
As the visionary founder of When in Roam, Demi conceived the idea and led the team with a creative and innovative approach. He invested significant time and effort in integrating crucial third-party APIs, powering the application's core functionalities with seamless integration.

**Christopher Marquand** - _Front-end Wizard and Feature Enhancer_<br>
Chris played a pivotal role in bringing When in Roam to life with stunning visual aesthetics and user-friendly interfaces. His expertise in CSS and front-end components elevated the overall user experience. When he wasn't busy styling components, he was thinking of new features to implement.

**Corissa Wehr** - _The Versatile Dynamo and Code Sleuth_<br>
Corissa demonstrated remarkable versatility by taking on multiple responsibilities and driving various aspects of the project with unwavering energy. Always ready to lend a helping hand, Corissa contributed to every feature across the application, bringing her dynamic skills to every challenge.

**MK Grissom** - _Back-end Guru & Documentation Maven_<br>
As a back-end expert, MK focused on building a robust foundation for When in Roam, ensuring seamless data handling and system reliability. Additionally, MK took charge of documenting the project, making it accessible to users and fellow developers alike.

## Design

- [API design](docs/apis.md)
- [GHI](docs/ghi.md)
