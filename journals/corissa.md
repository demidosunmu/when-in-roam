##### 07/24/2023 - 07/28/2023:

RSVP is functional, with upcoming/previous RSVPs showing up on profile page. Cleaned up code, fixed minor details, added globe to landing page. Encountered lots of issues with deployment -- fixed issues, app is now deployed!

##### 07/19/2023 - 07/20/2023:

Could not figure out how to set up edit profile. Decided to focus elsewhere and help the team troubleshoot their issues. Address autocomplete now working. Map is fully functional & events show up on map.

##### 07/17/2023 - 07/18/2023:

Broke (and fixed) API endpoints. Updated event list to use Redux, linked event host to logged in user, create event endpoint requires authentication.

##### 07/10/2023 - 07/14/202:

Began working on front-end forms and authentication. Team decided to incorporate Redux into application. Spent time getting familiar with it, then setting it up. Signup and login now functional on the frontend. Event Form not submitting properly -- spent a lot of time troubleshooting. Fixed bug in event form & updated landing page to have dropdown menu.

##### 07/03/2023:

Quick meeting to work on GitLab issue creation for the frontend.

##### 06/28/2023 - 06/30/2023:

Completed and tested all API endpoints. Began backend authentication for users & vendors, but could not figure it out. Decided to create a new branch to revert the code back to a working state. Used the new working branch to start fresh and decided to pivot away from having two types of users. We speculated that the JWTdown authentication source code needed to be implemented a certain way, which was causing problems. By the end of the day authentication was working and we could sign up, login, and logout on the backend.

##### 06/26/2023 - 06/27/2023:

Team discussed which database would be best to use (PostgreSQL vs MongoDB). Because we have established several relationships within our application, we decided it makes more sense to use PostgreSQL. Began GitLab issue creation & adding tables to the database.

##### 06/20/2023 - 06/23/2023:

Project ideation, MVP/stretch goals, wireframing & API design.
