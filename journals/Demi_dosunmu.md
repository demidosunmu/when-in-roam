7-28-23

Final Day. Worked on some last minute CSS changes. We got help from Delonte to properly deploy. We worked on what future versions may look like. Cleaned up any comments and unneeded code.

7-27-23
We spend the day workign on Deployment. I was unable to install Cirrus on my mac, but my teammates were able to. We got it to deploy partially thanks to help from Delonte. We're going to delete it and try to rebuild tomorrow

7-26-23
Continued working on EventMap.js and addressed final feedback before submitting the changes.
Continued working on CSS functionality.
Started reading up on Deployment

7-25-23
Continued working on EventMap.js.
Worked on CSS for the application
Other minor changes to the application.

7-24-23
Continued working on EventMap.js.

7-21-23
Continued working on EventMap.js.

7-20-23
Decided to scrap the current EventMap.js and started over. I was a little lost in the weeds, so maybe that's the best choice .

7-19-23
Continued working on EventMap.js.

7-18-23
Continued working on EventMap.js.

7-15-23
Continued working on the landing page, incorporating photos.

7-14-23
I added photos to the landing page.

7-13-23
I continued working on token authentication.

7-12-23
I continued working on token authentication.

7-9-23
I continued working on the Different Routers and APIs.

7-8-23
I continued working on the Different Routers and APIs.

7-6-23
I continued working on the Different Routers and APIs.

7-2-23
I worked on the LoginModal

7-1-23
I worked on styling to make the whole application look nicer.

6-30-23
The team and I met over the summer break to create the issues for the front end portion of the application. This includes the Signup page, LOGIN PAGE, Landing page, and others.

6-29-23
Spent a lot of time trying to figure out Authentication. Our application uses a user table, which would have a username and password. As the day progressed, we went back and forth discussing the pros and cons of having a separate Accounts table.

6-28-23
Implemented JwTdown for our application.

6-26-23
Created issues with the team in Gitlab and added tasks to those issues.
