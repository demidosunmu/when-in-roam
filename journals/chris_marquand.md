

June 27, 2023

Today marked the start of our  project to build a React App. We chose to use a SQL database. I took on the task of setting up the PostgreSQL database schema.

June 28, 2023

We focused on user authentication, and I helped the implementation of registration and login pages with JWT integration. However, we faced some compatibility issues with the backend.

June 29, 2023

Continuing our work on user authentication we managed to resolve by upgrading our server dependencies, we improved error handling and started building the user profile section. To streamline development, Corissa suggested using a React library for form validation.

June 30, 2023

While developing the user profile section, we encountered a performance bottleneck with profile picture uploads. We ended up abandoning the idea for later contemplation.

July 1, 2023

Today, we achieved a major milestone by implementing the "Create Post" feature and began work on the post feed. We encountered challenges with pagination, but we found a React package that efficiently handles this task.

July 4, 2023

Over the weekend, we continued refining the user posts' design and interactions. We dealt with some front-end bugs related to post deletion, which we resolved by reconfiguring our API endpoints.

July 5, 2023

We spent the day optimizing database queries and enhancing React component rendering. By implementing memoization and debouncing, we significantly improved the app's performance and reduced unnecessary API calls.

July 6, 2023

Today, we focused on adding user interactions to posts - likes, comments, and shares. We encountered real-time update issues but resolved them by integrating WebSocket communication between the front-end and back-end.

July 7, 2023

While refining user interactions, we discovered a potential security vulnerability. To mitigate it, we implemented input validation and sanitization, ensuring a more robust and secure application.

July 10, 2023

We dedicated today to thorough testing, identifying some edge cases and minor UI bugs. Using Jest and React Testing Library, we conducted comprehensive tests to ensure the app's stability.

July 11, 2023

After addressing the testing feedback, we deployed the React App and backend to separate servers. We faced deployment challenges due to environment configurations, which we resolved by using Docker containers.

July 12, 2023

Documentation took priority today as we documented the entire app, API endpoints, and database schema. We also added code comments to make the codebase more maintainable for future updates.

July 13, 2023

In our retrospective meeting, we discussed the blockers we encountered throughout the project, including communication gaps and occasional merge conflicts. To improve collaboration, we decided to hold daily stand-ups and follow version control best practices.

July 14, 2023

With the project completed, we conducted a final review of our work and ensured all tasks were completed as planned. Despite not launching the app publicly, we're satisfied with our progress and the knowledge gained from this student project.

July 17, 2023

In the past week, we performed additional testing and resolved some minor bugs. We also conducted a security audit, ensuring the app is well-protected against common vulnerabilities.

July 27, 2023

The student project has concluded, and we successfully built a functional React App with an SQL database. Though there won't be a public launch or user feedback, we're proud of our accomplishments and the experience gained in working on this project as a team.
