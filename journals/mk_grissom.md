### July 26, 2023

- I changed /profile to show cards for each event rather than a table row.
- I added an AboutUs to the Nav
- I created a README.md for our application
- I looked into deployment as we will have to figure that out asap

### July 25, 2023

- We finished unit tests
- I created a gender dropdown for Signup
- I did some code cleanup and got rid of comments and unused imports
- We spent a lot of time troubleshooting the redirect upon login.
- Corissa and I finally got that to work around 8:15 and logged off.

### July 24, 2023

- Today, we’re working on unit tests and finishing touches.
- Chris is working on the RSVP feature, which I will incorporate into the - profile page for upcoming and previous events.
- We spent the last part of the day each troubleshooting RSVP.
- Corrisa had another breakthrough.

### July 19, 2023

- Last night, Corissa fixed back end endpoints
- I fixed the way the front end was interacting with the endpoints.
- I finally figured out how to get actual account information from the state and into a user profile.
- I need to work on adding the upcoming and previous RSVP’s to the profile and add the edit view to the profile.
- Corissa and I spent the day working on the edit profile form.
- We could not get it to work with our current front-end endpoints. The backend endpoints are functional.
- We came up with a plan to revise the database table for account into one for account (username and password) and one to collect profile information.
- In the meantime, we need to move on to adding events to the View Profile page. This will need to be refactored for user information after the table changes.
- Perhaps, we should create an UpcomingRSVPs.js and a previous RSVPs.js and then import those into the profile later.

### July 18, 2023

- I worked on EditProfile and modified the language dropdown.

### July 17, 2023

- I finished Language dropdown and added it to Signup.
- Working on View/Edit Profile page
- ViewProfile works.
- I changed some of the accounts endpoints in api/routers.

### July 14, 2023

- Worked on Language Dropdown.

### July 13, 2023

- I spent the morning trying various solutions for a language menu and determined that I may have to do it the hard way.
- I put together the language selector and got a seemingly working menu.
- I then realized that we do not have an endpoint or query to update an account.
- I created the endpoint and matching query.

### July 12, 2023

- We began working on Redux as a group and worked on the signup form.
- We spent the evening troubleshooting the UI for Signup. When we clicked Signup, nothing appeared. After several hours of testing, Corissa discovered that adding a CSS rule for a pop-up allowed us to view the page.
- The type for languages was causing an issue, so we made the field optional for now.
- I will continue to work on Language tomorrow.

### July 11, 2023

- I started by working on the Edit Profile form in code rather than in theory.
- I researched how to handle the language menu. I tried importing ReactLanguageSelect from "react-languages-select", but this is outdated.
- I added options for the user's gender on the front-end exclusively on the edit profile page, but after my research, I think we may need to have separate files that determine which languages and genders are in the list. i.e. Languages.js and Genders.js. This would allow us to use them within multiple files, I think. We will need the same menus available in Create an Account.
- I'm journaling as I code, so I'm using this space to note that I need to further research how to not reset the values to blank but rather to the previous state for any item that was not changed. i.e. If language is updated but not first name, last name, or gender, I want to update the form to hold the new values for items that were updated and old values for items that were not. Currently, I have it set so that the form is blanked out upon submit.
- Candice sent me a CSV file with all the languages. I've approached that the wrong way a couple of times, so I'm still working on that.
- As a group, we have decided it may be a good time to incorporate Redux. We are taking the afternoon and evening to learn more.

### July 10, 2023

- I began to work on the front-end components for the user profile.
- I had to create an endpoint to get a single account.
- I had to spend some time researching how to create a user profile.
- I made some steady progress but decided to create the edit profile form first.
- I researched how to create a pop-up element for the edit profile page.

### July 3, 2023

- Corissa, Demi and I created issues for front-end components.

### June 30, 2023

- As a group, we spend the day working on Authentication.
- It was a day full of aha! moments
- Our biggest aha! moment was that the jwt-fastapi source code was looking specifically for accounts terminology.
- For this reason, we had to change our Users table, queries and routers to use accounts terminology.
- We re-visited our MVP and decided that we should pilot the application for users only.
- We can add vendor capability at a later stage of development.

### June 29, 2023

- As a group, we worked on Authentication.
- We ran into several issues due to our two different types of users (users and vendors)
- We created an accounts table that inserts a row when a user or vendor is created.

### June 28, 2023

- I created a checklist for our daily activities such as standups and pair assignments
- We worked on API endpoint testing as a group.
- I tested Create Event and Get All Events.
- I made changes to routers/events.py and queries/events.py so that endpoints were successful.
- I installed github copilot so I'm going to give that a try today.

### June 27, 2023

- I created migrations for database tables for users, vendors, and events with input from the group.
- Chris, Demi and I had trouble with the fastapi-1 volume and spent time troubleshooting
  - This was due to me overwriting the sample migration that contained dummy tables with our new tables.
  - Because I overwrote the old tables, there was now no way to drop them.
- As a group, we spent time troubleshooting Docker until all containers were running for all team members
  - Whitley and Yonah came to help troubleshoot.
  - It was unusual that 3/4 group members were having the issue when we had all pulled
  - As it turns out, there was something wrong with the ghi/node_modules
  - We deleted those, deleted all docker containers and images, rebuilt containers and images and then finally ran them.
  - After waiting for Docker to process everything, all containers were running without failing.
- By the end of day, each group member accessed database tables via pg-admin.
- We made plans to start authenication tomorrow.
