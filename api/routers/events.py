from fastapi import APIRouter, Depends, Response
from typing import Union, List
from queries.events import Error, EventIn, EventOut, EventQueries, EventUpdate
from authenticator import authenticator


router = APIRouter()


@router.post("/events", response_model=EventOut)
async def create_event(
    event: EventIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: EventQueries = Depends(),
):
    if not account_data:
        pass

    host_id = account_data.get("id")

    created_event = repo.create_event(host_id, event)

    return created_event


@router.get("/events/", response_model=Union[List[EventOut], Error])
def get_all_events(repo: EventQueries = Depends()):
    return repo.get_all_events()


@router.get("/events/{event_id}", response_model=EventOut)
def get_event(
    event_id: int,
    response: Response,
    repo: EventQueries = Depends(),
) -> EventOut:
    event = repo.get_event(event_id)
    if event is None:
        response.status_code = 404
    return event


@router.put("/events/{event_id}", response_model=Union[Error, EventOut])
def update_event(
    event_id: int,
    event: EventUpdate,
    repo: EventQueries = Depends(),
) -> Union[Error, EventOut]:
    return repo.update_event(event_id, event)


@router.delete("/events/{event_id}", response_model=bool)
def delete_event(
    event_id: int,
    repo: EventQueries = Depends(),
) -> bool:
    return repo.delete_event(event_id)
