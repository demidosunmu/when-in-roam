from fastapi import APIRouter, Depends
from queries.rsvp import RSVPQueries
from authenticator import authenticator
from queries.events import EventQueries
from queries.accounts import AccountsQueries

router = APIRouter()


@router.get("/events/{event_id}/rsvp")
async def get_rsvps_for_event(
    event_id: int,
    repo: RSVPQueries = Depends(),
    event_repo: EventQueries = Depends(),
):
    event = event_repo.get_event(event_id)
    if not event:
        return {"message": "Event not found"}

    rsvps = repo.get_rsvps_by_event_id(event_id)
    if not rsvps:
        return []

    return rsvps


@router.get("/rsvps/{user_id}")
async def get_rsvps_for_user(
    user_id: int,
    repo: RSVPQueries = Depends(),
    account_repo: AccountsQueries = Depends(),
):
    account = account_repo.get_by_id(user_id)
    print(f"Account data: {account}")
    if not account:
        return {"message": "User not found"}

    rsvps = repo.get_rsvps_by_user_id(user_id)
    if not rsvps:
        return []

    return rsvps


@router.post("/events/{event_id}/rsvp")
async def create_rsvp(
    event_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: RSVPQueries = Depends(),
):
    if not account_data:
        pass

    user_id = account_data.get("id")
    if repo.create_rsvp(user_id, event_id):
        return {"message": "RSVP successfully created"}
    else:
        return {"error": "Could not create RSVP"}


@router.delete("/events/rsvp/{rsvp_id}")
async def cancel_rsvp(
    rsvp_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: RSVPQueries = Depends(),
):
    if not account_data:
        return {"error": "Unauthorized access"}

    rsvp = repo.get_rsvp_by_id(rsvp_id)
    if not rsvp or rsvp["user_id"] != account_data.get("id"):
        return {"error": "Could not find that RSVP"}

    if repo.cancel_rsvp(rsvp_id):
        return {"message": "RSVP successfully cancelled"}
    else:
        return {"error": "Could not cancel RSVP"}
