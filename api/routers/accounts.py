from fastapi import Depends, Response, APIRouter, Request
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from pydantic import BaseModel
from queries.accounts import (
    AccountIn,
    AccountOut,
    AccountsQueries,
    Error,
)
from typing import Union, List
from fastapi import HTTPException
import psycopg


class AccountForm(BaseModel):
    first_name: str
    last_name: str
    birthday: str
    gender: str
    language: list
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.post("/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    repo: AccountsQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = repo.create(info, hashed_password)
    except psycopg.errors.UniqueViolation:
        raise HTTPException(
            status_code=409,
            detail="An account with this email already exists",
        )
    form = AccountForm(
        first_name=info.first_name,
        last_name=info.last_name,
        birthday=info.birthday,
        gender=info.gender,
        language=info.language,
        username=info.email,
        password=info.password,
    )
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.get("/accounts/", response_model=Union[List[AccountOut], Error])
def get_all_accounts(repo: AccountsQueries = Depends()):
    return repo.get_all_accounts()


@router.get("/accounts/{email}", response_model=AccountOut)
def get_account(
    email: str,
    response: Response,
    repo: AccountsQueries = Depends(),
) -> AccountOut:
    account = repo.get_one(email)
    if account is None:
        response.status_code = 404
    return account


@router.get("/accounts/{user_id}/by-id", response_model=AccountOut)
def get_account_by_id(
    user_id: int,
    repo: AccountsQueries = Depends(),
) -> AccountOut:
    account = repo.get_by_id(user_id)
    if account is None:
        raise HTTPException(status_code=404, detail="User not found")
    return account
