from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import accounts, events, rsvp
from authenticator import authenticator
import os

app = FastAPI()
app.include_router(accounts.router)
app.include_router(events.router)
app.include_router(rsvp.router)
app.include_router(authenticator.router)

origins = [
    os.environ.get("CORS_HOST"),
    "https://localhost:3000",
    "https://vagabond7103425.gitlab.io/when-in-roam/",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }
