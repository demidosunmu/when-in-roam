from fastapi.testclient import TestClient
from main import app
from queries.events import EventQueries

client = TestClient(app)


class MockEventQueries:
    def get_event(self, event_id):
        if event_id == 1:
            return {
                "id": 1,
                "name": "Beyonce Concert",
                "date": "08/23/2023",
                "address": "1 AMB Dr NW, Atlanta, GA 30313, United States",
                "latitude": 33.7553,
                "longitude": 84.4006,
                "description": "Beyonce Final Show on Rennaisance Tour",
                "minimum_attendees": 10,
                "maximum_attendees": 30,
                "fee": 500,
                "minimum_age": 18,
                "requirements": "Dancing Shoes & Tickets",
                "pet_friendly": False,
            }
        else:
            return None

    def delete_event(self, event_id):
        if event_id == 1:
            return True
        else:
            return False


def test_delete_event():
    app.dependency_overrides[EventQueries] = MockEventQueries
    response = client.delete("/events/1")
    app.dependency_overrides = {}
    assert response.status_code == 200
    assert response.json() is True
