from fastapi.testclient import TestClient
from main import app
from queries.events import EventQueries

client = TestClient(app)


# Arrange / Act / Assert
# Set up / Enact / Assert / Teardown
class EmptyEventQueries:
    def get_events(self):
        return []


class FakeEventQueries:
    def get_all_events(self):
        return [
            {
                "id": 1,
                "name": "Beyonce Concert",
                "start_date": "2023-07-27T17:31:30",
                "end_date": "2023-07-27T17:32:30",
                "address": "1 AMB Dr NW, Atlanta, GA 30313, United States",
                "latitude": 33.7553,
                "longitude": 84.4006,
                "description": "Beyonce Final Show on Rennaisance Tour",
                "minimum_attendees": 10,
                "maximum_attendees": 30,
                "fee": 500,
                "minimum_age": 18,
                "requirements": "Dancing Shoes & Tickets",
                "pet_friendly": False,
            },
            {
                "id": 2,
                "name": "Drake Concert",
                "start_date": "2023-07-27T17:31:30",
                "end_date": "2023-07-27T17:32:30",
                "address": "1 AMB Dr NW, Atlanta, GA 30313, United States",
                "latitude": 33.7553,
                "longitude": 84.4006,
                "description": "Drake Final Tour",
                "minimum_attendees": 10,
                "maximum_attendees": 30,
                "fee": 500,
                "minimum_age": 18,
                "requirements": "Dancing Shoes & Tickets",
                "pet_friendly": False,
            },
        ]


def test_get_all_events():
    # Arrange
    app.dependency_overrides[EventQueries] = FakeEventQueries
    # Act
    response = client.get("/events/")
    expected = [
        {
            "id": 1,
            "name": "Beyonce Concert",
            "start_date": "2023-07-27T17:31:30",
            "end_date": "2023-07-27T17:32:30",
            "address": "1 AMB Dr NW, Atlanta, GA 30313, United States",
            "latitude": 33.7553,
            "longitude": 84.4006,
            "description": "Beyonce Final Show on Rennaisance Tour",
            "minimum_attendees": 10,
            "maximum_attendees": 30,
            "fee": 500,
            "minimum_age": 18,
            "requirements": "Dancing Shoes & Tickets",
            "pet_friendly": False,
        },
        {
            "id": 2,
            "name": "Drake Concert",
            "start_date": "2023-07-27T17:31:30",
            "end_date": "2023-07-27T17:32:30",
            "address": "1 AMB Dr NW, Atlanta, GA 30313, United States",
            "latitude": 33.7553,
            "longitude": 84.4006,
            "description": "Drake Final Tour",
            "minimum_attendees": 10,
            "maximum_attendees": 30,
            "fee": 500,
            "minimum_age": 18,
            "requirements": "Dancing Shoes & Tickets",
            "pet_friendly": False,
        },
    ]
    # Clean up
    app.dependency_overrides = {}
    # Assert
    assert response.status_code == 200
    assert response.json() == expected
