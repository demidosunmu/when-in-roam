# update_events_test.py

from fastapi.testclient import TestClient
from main import app
from queries.events import EventQueries
from typing import Dict
import pytest
from authenticator import authenticator

client = TestClient(app)


class UpdateEventQueries:
    def update_event(self, event_id, update_data):
        event = {
            "id": event_id,
            "name": "Event",
            "start_date": "2023-07-27T17:31:30",
            "end_date": "2023-07-27T17:32:30",
            "address": "Address",
            "latitude": 33.7553,
            "longitude": 84.4006,
            "description": "Description",
            "minimum_attendees": 5,
            "maximum_attendees": 20,
            "fee": 250,
            "minimum_age": 21,
            "requirements": "Requirements",
            "pet_friendly": True,
        }
        event.update(update_data)
        return event


def mock_get_current_account_data():
    return {"username": "test_user"}


def mock_try_get_current_account_data():
    return {"username": "test_user"}


@pytest.fixture(autouse=True)
def override_dependencies():
    app.dependency_overrides[EventQueries] = UpdateEventQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = mock_get_current_account_data
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_try_get_current_account_data
    yield
    app.dependency_overrides.clear()


def test_update_event():
    # Arrange
    event_data: Dict = {
        "name": "Updated Event",
        "start_date": "2023-07-27T17:31:30",
        "end_date": "2023-07-27T17:32:30",
        "address": "Updated Address",
        "latitude": 33.7553,
        "longitude": 84.4006,
        "description": "Updated Description",
        "minimum_attendees": 5,
        "maximum_attendees": 20,
        "fee": 250,
        "minimum_age": 22,
        "requirements": "Updated Requirements",
        "pet_friendly": True,
    }

    client.cookies[authenticator.cookie_name] = "test_access_token"

    # Act - Replace 1 with the actual event ID that you want to update.
    response = client.put(
        "/events/1",
        json=event_data,
    )
    # Assert
    assert response.status_code == 200
    assert response.json() == {**event_data, "id": 1}
