from fastapi.testclient import TestClient
from main import app
from queries.events import EventQueries
from typing import Dict
import pytest
from authenticator import authenticator
import pprint

client = TestClient(app)


class CreateEventQueries:
    def create_event(self, account, event):
        result = {
            "id": 1,
            "name": "Beyonce Concert",
            "start_date": "2023-07-27T17:31:30",
            "end_date": "2023-07-27T17:32:30",
            "address": "1 AMB Dr NW, Atlanta, GA 30313, United States",
            "latitude": 33.7553,
            "longitude": 84.4006,
            "description": "Beyonce Final Show on Rennaisance Tour",
            "minimum_attendees": 10,
            "maximum_attendees": 30,
            "fee": 500,
            "minimum_age": 18,
            "requirements": "Dancing Shoes & Tickets",
            "pet_friendly": False,
        }
        result.update(event)
        return result


def mock_get_current_account_data():
    return {"username": "test_user"}


def mock_try_get_current_account_data():
    return {"username": "test_user"}


@pytest.fixture(autouse=True)
def override_dependencies():
    app.dependency_overrides[EventQueries] = CreateEventQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = mock_get_current_account_data
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_try_get_current_account_data
    yield
    app.dependency_overrides.clear()


def test_create_event():
    # Arrange
    event_data: Dict = {
        "name": "Beyonce Concert",
        "start_date": "2023-07-27T17:31:30",
        "end_date": "2023-07-27T17:32:30",
        "address": "1 AMB Dr NW, Atlanta, GA 30313, United States",
        "latitude": 33.7553,
        "longitude": 84.4006,
        "description": "Beyonce Final Show on Rennaisance Tour",
        "minimum_attendees": 10,
        "maximum_attendees": 30,
        "fee": 500,
        "minimum_age": 18,
        "requirements": "Dancing Shoes & Tickets",
        "pet_friendly": False,
    }

    client.cookies[authenticator.cookie_name] = "test_access_token"

    response = client.post(
        "/events",
        json=event_data,
    )

    # Print the actual response data
    print("Actual Response Data:")
    pprint.pprint(response.json())

    # Print the expected data
    print("Expected Data:")
    pprint.pprint({**event_data, "id": 1})

    assert response.status_code == 200
    assert response.json() == {**event_data, "id": 1}
