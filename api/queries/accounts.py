from pydantic import BaseModel
from queries.pool import pool
from typing import Optional, Union, List


class Error(BaseModel):
    message: str


class Account(BaseModel):
    id: int
    first_name: str
    last_name: str
    birthday: str
    gender: str
    language: list
    email: str
    hashed_password: str


class AccountIn(BaseModel):
    first_name: str
    last_name: str
    birthday: str
    gender: str
    language: list
    email: str
    password: str


class AccountUpdate(BaseModel):
    first_name: Optional[str]
    last_name: Optional[str]
    gender: Optional[str]
    language: Optional[list]
    email: Optional[str]


class AccountOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    birthday: str
    gender: str
    language: list
    email: str


class AccountsQueries:
    def create(self, account: AccountIn, hashed_password: str) -> Account:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO accounts
                        (first_name,
                        last_name,
                        birthday,
                        gender,
                        language,
                        email,
                        hashed_password)
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                    (
                        account.first_name,
                        account.last_name,
                        account.birthday,
                        account.gender,
                        account.language,
                        account.email,
                        hashed_password,
                    ),
                )
                id = result.fetchone()[0]
                return Account(
                    id=id,
                    first_name=account.first_name,
                    last_name=account.last_name,
                    birthday=account.birthday,
                    gender=account.gender,
                    language=account.language,
                    email=account.email,
                    hashed_password=hashed_password,
                )

    def get_all_accounts(self) -> Union[Error, List[AccountOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                        first_name,
                        last_name,
                        birthday,
                        gender,
                        language,
                        email,
                        hashed_password
                        FROM accounts
                        ORDER BY last_name;
                        """
                    )
                    return [
                        self.record_to_account_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve accounts"}

    def get_one(self, email: str) -> Optional[AccountOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , first_name
                            , last_name
                            , birthday
                            , gender
                            , language
                            , email
                            , hashed_password
                        FROM accounts
                        WHERE email = %s;
                        """,
                        [email],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_account_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not access this account"}

    def get_by_id(self, user_id: int) -> Optional[AccountOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , first_name
                            , last_name
                            , birthday
                            , gender
                            , language
                            , email
                            , hashed_password
                        FROM accounts
                        WHERE id = %s;
                        """,
                        [user_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_account_out(record)
        except Exception as e:
            print(e)
            return None

    def record_to_account_out(self, record):
        return Account(
            id=record[0],
            first_name=record[1],
            last_name=record[2],
            birthday=record[3],
            gender=record[4],
            language=record[5],
            email=record[6],
            hashed_password=record[7],
        )
