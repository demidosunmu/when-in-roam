from pydantic import BaseModel
from queries.pool import pool
from typing import List, Union, Optional
from decimal import Decimal
from datetime import datetime
from pydantic import Field


class Error(BaseModel):
    message: str


class EventIn(BaseModel):
    name: str
    start_date: str
    end_date: str
    address: str
    latitude: float
    longitude: float
    description: str
    minimum_attendees: int
    maximum_attendees: int
    fee: Decimal
    minimum_age: int
    requirements: str
    pet_friendly: bool = Field(default=False)


class EventUpdate(BaseModel):
    name: Optional[str]
    start_date: Optional[str]
    end_date: Optional[str]
    address: Optional[str]
    latitude: Optional[float]
    longitude: Optional[float]
    description: Optional[str]
    minimum_attendees: Optional[int]
    maximum_attendees: Optional[int]
    fee: Optional[Decimal]
    minimum_age: Optional[int]
    requirements: Optional[str]
    pet_friendly: Optional[bool]


class EventOut(BaseModel):
    id: int
    name: str
    start_date: datetime
    end_date: datetime
    address: str
    latitude: float
    longitude: float
    description: str
    minimum_attendees: int
    maximum_attendees: int
    fee: Decimal
    minimum_age: int
    requirements: str
    pet_friendly: bool


class EventQueries:
    def create_event(self, host_id: int, event: EventIn) -> EventOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO events
                        (host,
                        name,
                        start_date,
                        end_date,
                        address,
                        latitude,
                        longitude,
                        description,
                        minimum_attendees,
                        maximum_attendees,
                        fee,
                        minimum_age,
                        requirements,
                        pet_friendly)
                        VALUES
                            (
                                %s,
                                %s,
                                %s,
                                %s,
                                %s,
                                %s,
                                %s,
                                %s,
                                %s,
                                %s,
                                %s,
                                %s,
                                %s,
                                %s)

                        RETURNING id
                        """,
                    (
                        host_id,
                        event.name,
                        event.start_date,
                        event.end_date,
                        event.address,
                        event.latitude,
                        event.longitude,
                        event.description,
                        event.minimum_attendees,
                        event.maximum_attendees,
                        event.fee,
                        event.minimum_age,
                        event.requirements,
                        event.pet_friendly,
                    ),
                )
                id = result.fetchone()[0]
                old_data = event.dict()
                return EventOut(id=id, **old_data)

    def get_all_events(self) -> Union[Error, List[EventOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                        name,
                        start_date,
                        end_date,
                        address,
                        latitude,
                        longitude,
                        description,
                        minimum_attendees,
                        maximum_attendees,
                        fee,
                        minimum_age,
                        requirements,
                        pet_friendly
                        FROM events
                        ORDER BY id;
                        """
                    )
                    return [
                        self.record_to_event_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve events"}

    def get_event(self, event_id: int) -> Optional[EventOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                            name,
                            start_date,
                            end_date,
                            address,
                            latitude,
                            longitude,
                            description,
                            minimum_attendees,
                            maximum_attendees,
                            fee,
                            minimum_age,
                            requirements,
                            pet_friendly
                        FROM events
                        Where id = %s;
                        """,
                        [event_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_event_out(record)
        except Exception:
            return {"message": "Could not access this event"}

    def update_event(
        self, event_id: int, event: EventIn
    ) -> Union[EventOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    existing_event = self.get_event(event_id)
                    if existing_event is None:
                        return {"message": "Event does not exist"}
                    updated_fields = event.dict(exclude_unset=True)
                    for key, value in updated_fields.items():
                        setattr(existing_event, key, value)
                    db.execute(
                        """
                        UPDATE events
                        SET name = %s
                        , start_date = %s
                        , end_date = %s
                        , address = %s
                        , latitude = %s
                        , longitude = %s
                        , description = %s
                        , minimum_attendees = %s
                        , fee = %s
                        , minimum_age = %s
                        , requirements = %s
                        , pet_friendly = %s
                        WHERE id = %s
                        """,
                        [
                            existing_event.name,
                            existing_event.start_date,
                            existing_event.end_date,
                            existing_event.address,
                            existing_event.latitude,
                            existing_event.longitude,
                            existing_event.description,
                            existing_event.minimum_attendees,
                            existing_event.fee,
                            existing_event.minimum_age,
                            existing_event.requirements,
                            existing_event.pet_friendly,
                            event_id,
                        ],
                    )
                return EventOut(**existing_event.dict())
        except Exception:
            return {"message": "Could not update this event"}

    def delete_event(self, event_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM events
                        WHERE id = %s
                        """,
                        [event_id],
                    )
                    return True
        except Exception:
            return False

    def record_to_event_out(self, record):
        return EventOut(
            id=record[0],
            name=record[1],
            start_date=record[2],
            end_date=record[3],
            address=record[4],
            latitude=record[5],
            longitude=record[6],
            description=record[7],
            minimum_attendees=record[8],
            maximum_attendees=record[9],
            fee=float(record[10].replace("$", "")),
            minimum_age=record[11],
            requirements=record[12],
            pet_friendly=record[13],
        )
