from queries.pool import pool
from typing import Optional


class RSVPQueries:
    def create_rsvp(self, user_id: int, event_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO rsvps (user_id, event_id)
                        VALUES (%s, %s)
                        """,
                        (user_id, event_id),
                    )
                conn.commit()
                return True
        except Exception:
            return False

    def cancel_rsvp(self, rsvp_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM rsvps
                        WHERE id = %s
                        """,
                        (rsvp_id,),
                    )
                conn.commit()
                return True
        except Exception:
            return False

    def get_rsvp_by_id(self, rsvp_id: int) -> Optional[dict]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT * FROM rsvps
                        WHERE id = %s
                        """,
                        (rsvp_id,),
                    )
                    rsvp = db.fetchone()
            if rsvp is None:
                return None
            return {"id": rsvp[0], "event_id": rsvp[1], "user_id": rsvp[2]}
        except Exception as e:
            print(f"Error getting RSVP by ID: {e}")
            return None

    def get_rsvps_by_event_id(self, event_id: int) -> Optional[list[dict]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT rsvps.id,
                        rsvps.event_id,
                        rsvps.user_id,
                        accounts.first_name,
                        accounts.last_name,
                        accounts.gender

                        FROM rsvps
                        JOIN accounts ON rsvps.user_id = accounts.id
                        WHERE rsvps.event_id = %s
                        """,
                        (event_id,),
                    )
                    rsvps = db.fetchall()
            if not rsvps:
                return None
            return [
                {
                    "id": rsvp[0],
                    "event_id": rsvp[1],
                    "user_id": rsvp[2],
                    "user_first_name": rsvp[3],
                    "user_last_name": rsvp[4],
                    "user_gender": rsvp[5],
                }
                for rsvp in rsvps
            ]
        except Exception as e:
            print(f"Error getting RSVPs by event ID: {e}")
            return None

    def get_rsvps_by_user_id(self, user_id: int) -> Optional[list[dict]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT rsvps.id,
                        rsvps.event_id,
                        rsvps.user_id,
                        events.name,
                        events.start_date,
                        events.end_date,
                        accounts.first_name,
                        accounts.last_name,
                        accounts.gender
                        FROM rsvps
                        JOIN events ON rsvps.event_id = events.id
                        JOIN accounts ON rsvps.user_id = accounts.id
                        WHERE rsvps.user_id = %s
                        """,
                        (user_id,),
                    )
                    rsvps = db.fetchall()
            if not rsvps:
                return None
            return [
                {
                    "id": rsvp[0],
                    "event_id": rsvp[1],
                    "user_id": rsvp[2],
                    "event_name": rsvp[3],
                    "event_start_date": rsvp[4],
                    "event_end_date": rsvp[5],
                    "user_first_name": rsvp[6],
                    "user_last_name": rsvp[7],
                    "user_gender": rsvp[8],
                }
                for rsvp in rsvps
            ]
        except Exception as e:
            print(f"Error getting RSVPs by user ID: {e}")
            return None
