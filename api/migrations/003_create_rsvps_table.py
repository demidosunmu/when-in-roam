steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE rsvps (
            id SERIAL PRIMARY KEY NOT NULL,
            event_id INTEGER NOT NULL,
            user_id INTEGER NOT NULL,
            FOREIGN KEY (event_id) REFERENCES events (id),
            FOREIGN KEY (user_id) REFERENCES accounts (id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE rsvps;
        """,
    ]
]
