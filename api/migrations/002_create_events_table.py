steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE events (
            id SERIAL PRIMARY KEY NOT NULL,
            host VARCHAR(30),
            name VARCHAR(100) NOT NULL,
            start_date TIMESTAMP NOT NULL, -- new field for event start date and time
            end_date TIMESTAMP NOT NULL, -- new field for event end date and time
            address TEXT NOT NULL,
            latitude NUMERIC(9,6),
            longitude NUMERIC(9,6),
            description TEXT NOT NULL,
            minimum_attendees SMALLINT NOT NULL,
            maximum_attendees SMALLINT NOT NULL,
            fee MONEY NOT NULL,
            minimum_age SMALLINT NOT NULL,
            requirements TEXT NOT NULL,
            pet_friendly BOOLEAN NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE events;
        """,
    ]
]
