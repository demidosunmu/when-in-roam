### Description:

This section is a high level overview of what this task includes i.e. a user story.

### Story

Stories are written to realize functionality defined by a Feature

As a [role]
I want [action]
So that [consequence]

### Feature

Given []
When []
Then []
And []

### Acceptance Criteria

Acceptance criteria is one or more statements that are clear, concise, testable, understandable, and written from a user's perspective

- []
- []
- []

### Definition of Done:

DoD is a list of deliverables that are required to complete this task.

- [ ] Documentation (if needed)
- [ ] Source code
- [ ] Unit test(s) (if needed)
- [ ] Passing CI/CD pipeline(s)
- [ ] Approved and completed merge request(s)

### Technical Description

### Screenshots
