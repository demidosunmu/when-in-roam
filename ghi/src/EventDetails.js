import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  useGetEventQuery,
  useGetRSVPsQuery,
  useCreateRSVPMutation,
  useDeleteRSVPMutation,
  useGetTokenQuery,
} from "./app/api";
import Notification from "./Notification";
import { useParams, useNavigate } from "react-router-dom";
import { setHasRSVPed } from "./app/rsvpSlice";
import { formatDateTime } from "./dateUtils";
import "./event-D.css";

function EventDetails() {
  const { eventId } = useParams();
  const { data: event, error, isLoading } = useGetEventQuery(eventId);
  const { data: tokenData } = useGetTokenQuery();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const accountId = tokenData?.account?.id;
  const { data: rsvpData } = useGetRSVPsQuery(eventId);
  const hasRSVPed = useSelector((state) => state.rsvp.hasRSVPed);
  const { refetch: refetchRSVPs } = useGetRSVPsQuery(eventId);
  const [createRSVP, { isSuccess: isRSVPSuccess, isLoading: isRSVPLoading }] =
    useCreateRSVPMutation();
  const [
    deleteRSVP,
    { isSuccess: isUnRSVPSuccess, isLoading: isUnRSVPLoading },
  ] = useDeleteRSVPMutation();
  const rsvpCount = rsvpData?.length || 0;
  console.log("rsvpData:", rsvpData);

  useEffect(() => {
    if (rsvpData) {
      const userRSVP = rsvpData.find((rsvp) => rsvp.user_id === accountId);
      dispatch(setHasRSVPed(!!userRSVP));
    }
  }, [rsvpData, accountId, dispatch]);

  useEffect(() => {
    if (isRSVPSuccess || isUnRSVPSuccess) {
    }
  }, [isRSVPSuccess, isUnRSVPSuccess, eventId]);

  const handleRSVP = async () => {
    if (hasRSVPed) {
      const rsvpId = rsvpData.find((rsvp) => rsvp.user_id === accountId)?.id;
      if (rsvpId) {
        await deleteRSVP(rsvpId);
        refetchRSVPs();
      }
    } else {
      await createRSVP({ accountId, eventId });
      refetchRSVPs();
    }
  };
  const isRSVPDisabled = isRSVPLoading || isUnRSVPLoading;

  const renderRsvp = () => {
    if (!rsvpData || rsvpData.length === 0) {
      return <p>No one has RSVP'd to this event yet.</p>;
    }
    return (
      <div>
        <p>Who's coming along?</p>
        <ul>
          {rsvpData.map((rsvp) => (
            <li key={rsvp.id}>
              {rsvp.user_first_name} {rsvp.user_last_name} | {rsvp.user_gender}
            </li>
          ))}
        </ul>
      </div>
    );
  };

  if (isLoading) {
    return <div className="container">Loading...</div>;
  }

  if (error) {
    return (
      <div className="container">
        <Notification type="danger">
          {error.data.detail.map((err, index) => (
            <div key={index}>
              <strong>Error {index + 1}:</strong>
              <div>Location: {err.loc.join(", ")}</div>
              <div>Message: {err.msg}</div>
              <div>Type: {err.type}</div>
            </div>
          ))}
        </Notification>
      </div>
    );
  }

  if (!event) {
    return <div className="container">Event not found</div>;
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-8">
          <div className="cards-container">
            <div className="card mb-3">
              <div className="card-header">{event.name}</div>
              <div className="card-body">
                <p>
                  <strong>Address:</strong> {event.address}
                </p>
                <p>
                  <strong>Date/Time:</strong> {formatDateTime(event.start_date)}
                </p>
              </div>
            </div>
            <div className="card mb-3">
              <div className="card-header">Event Description</div>
              <div className="card-body">{event.description}</div>
            </div>
            <div className="card mb-3">
              <div className="card-header">Event Details</div>
              <div className="card-body">
                <p>
                  <strong>Minimum Attendees:</strong> {event.minimum_attendees}
                </p>
                <p>
                  <strong>Maximum Attendees:</strong> {event.maximum_attendees}
                </p>
                <p>
                  <strong>Fee:</strong> ${parseFloat(event.fee).toFixed(2)}
                </p>
                <p>
                  <strong>Minimum Age:</strong> {event.minimum_age}
                </p>
                <p>
                  <strong>Requirements:</strong> {event.requirements}
                </p>
                <p>
                  <strong>Pet Friendly:</strong>{" "}
                  {event.pet_friendly ? "Yes" : "No"}
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="col-md-4">
          <div className="button-container mb-3">
            {" "}
            <button
              className="btn btn-primary"
              onClick={handleRSVP}
              disabled={isRSVPDisabled}
            >
              {hasRSVPed ? "Cancel RSVP" : "RSVP"}
            </button>
            <button
              type="button"
              className="btn btn-secondary"
              onClick={() => navigate("/events")}
            >
              Return to Events
            </button>
          </div>

          <div className="card mb-3 card-4">
            <div className="card-header">Who's Coming Along?</div>
            <div className="card-body">
              <p>Number of RSVPs: {rsvpCount}</p>
              {renderRsvp()}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default EventDetails;
