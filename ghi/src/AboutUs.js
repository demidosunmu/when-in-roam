import React from "react";
import "./App.css";

function AboutUs({ onClose }) {
  return (
    <div className="popup-container">
      <div className="popup-content">
        <h2>About Us</h2>
        <p>
          Welcome to When in Roam, your go-to social networking platform for
          travelers and expatriates alike. Our mission is to connect individuals
          in new places, fostering meaningful connections and unforgettable
          experiences.
        </p>
        <h3>Key Features:</h3>
        <h5>Event Creation</h5>
        <p>
          {" "}
          Create exciting events based on your interests and preferences, from
          casual meetups to adventurous group activities.
        </p>
        <h5>RSVP System</h5>
        <p>
          Discover events organized by others in your area and indicate your
          interest in participating.
        </p>
        <h5>Map Feature</h5>
        <p>
          {" "}
          Explore a user-friendly map displaying all events nearby, making it
          easy to find exciting activities and connect with like-minded
          individuals.
        </p>
        <h5>Attend and Connect</h5>
        <p>
          Meet up with fellow users at scheduled events, forging a sense of
          community and friendship in unfamiliar territories.
        </p>
        <p>
          Whether you're a traveler exploring distant lands or an expat seeking
          to build connections in your adopted community, When in Roam is here
          to enhance your journey. Join us today and be a part of a global
          network that makes the world feel a little smaller and a lot
          friendlier.
        </p>
        <button onClick={onClose}>Close</button>
      </div>
    </div>
  );
}

export default AboutUs;
