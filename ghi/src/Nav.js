import React, { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { useGetTokenQuery, useLogOutMutation } from "./app/api";
import { useDispatch } from "react-redux";
import { showModal, LOG_IN_MODAL, SIGN_UP_MODAL } from "./app/accountSlice";
import logo from "./logo.svg";
import LogInModal from "./LogInModal";
import SignUpModal from "./SignUpModal";
import { useEffect } from "react";
import AboutUs from "./AboutUs";
import "./App.css";

function LoginButtons(props) {
  const dispatch = useDispatch();
  const classNames = `vertical-buttons ${props.show ? "" : "is-hidden"}`;

  return (
    <div className={classNames}>
      <button
        onClick={() => {
          dispatch(showModal(SIGN_UP_MODAL));
        }}
        className="button"
      >
        <strong>Sign up</strong>
      </button>
      <button
        onClick={() => dispatch(showModal(LOG_IN_MODAL))}
        className="button"
      >
        Log in
      </button>
      <div className="button">
        <button onClick={props.onAboutClick}>About Us</button>
      </div>
    </div>
  );
}

function LogoutButtons() {
  const navigate = useNavigate();
  const [logOut, { data }] = useLogOutMutation();

  useEffect(() => {
    if (data) {
      navigate("/");
    }
  }, [data, navigate]);

  return (
    <div className="vertical-buttons">
      <NavLink to="/profile">
        <button>My Profile</button>
      </NavLink>
      <NavLink to="/events">
        <button>Events</button>
      </NavLink>
      <button
        onClick={async () => {
          await logOut();
          navigate("/");
        }}
      >
        Log out
      </button>
    </div>
  );
}

function Nav() {
  const { data: token, isLoading: tokenLoading } = useGetTokenQuery();
  const [isMenuOpen, setMenuOpen] = useState(false);
  const [showAboutPopup, setShowAboutPopup] = useState(false);
  const navigate = useNavigate();

  const toggleMenu = () => {
    setMenuOpen(!isMenuOpen);
  };

  const handleItemClick = () => {
    setMenuOpen(false);
  };

  const handleAboutPopup = () => {
    setShowAboutPopup(true);
  };

  const handleClosePopup = () => {
    setShowAboutPopup(false);
  };

  return (
    <>
      <nav
        className="navbar is-fixed-top"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <NavLink className="navbar-item" to="/">
            <img src={logo} height="86" width="43" alt="" />
          </NavLink>
        </div>

        <div className="navbar-header">
          <h1 className="navbar-title" onClick={() => navigate("/")}>
            When In Roam
          </h1>
          <p className="navbar-slogan">Travel. Explore. Roam.</p>
        </div>
        <div className="navbar-end">
          <button className="navbar-burger" onClick={toggleMenu}>
            <span></span>
            <span></span>
            <span></span>
          </button>
          <div className="navbar-dropdown-wrapper">
            {isMenuOpen && (
              <div className="navbar-dropdown" onClick={handleItemClick}>
                {tokenLoading ? (
                  <LoginButtons show={false} onAboutClick={handleAboutPopup} />
                ) : token ? (
                  <LogoutButtons />
                ) : (
                  <LoginButtons show={true} onAboutClick={handleAboutPopup} />
                )}
              </div>
            )}
          </div>
          {showAboutPopup && <AboutUs onClose={handleClosePopup} />}
        </div>
      </nav>

      <LogInModal />
      <SignUpModal />
    </>
  );
}

export default Nav;
