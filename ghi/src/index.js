import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import App from "./App";
import Nav from "./Nav";
import EventForm from "./EventForm";
import EventList from "./EventList";
import EventDetails from "./EventDetails";
import ViewProfile from "./ViewProfile";
import "./index.css";
import { store } from "./app/store";
import { Provider } from "react-redux";
import EventMap from "./EventMap";
import { LoadScript } from "@react-google-maps/api";

const root = ReactDOM.createRoot(document.getElementById("root"));
const domain = /https:\/\/[^/]+/;
const basename = process.env.PUBLIC_URL.replace(domain, "");
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter basename={basename}>
        <Nav />
        <LoadScript
          googleMapsApiKey={process.env.REACT_APP_GOOGLE_MAPS_API_KEY}
          libraries={["places"]}
        >
          <Routes>
            <Route path="/" element={<App />} />
            <Route path="events">
              <Route path="" element={<EventMap />} />
              <Route path="create" element={<EventForm />} />
              <Route path="/events/:eventId" element={<EventDetails />} />
            </Route>
            <Route path="events/list" element={<EventList />} />
            <Route path="profile">
              <Route path="" element={<ViewProfile />} />
            </Route>
          </Routes>
        </LoadScript>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>
);
