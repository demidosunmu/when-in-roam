import React, { useEffect, useRef } from "react";
import * as THREE from "three";

function App() {
  const mountRef = useRef(null);

  useEffect(() => {
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      1000
    );

    const renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.shadowMap.enabled = true;
    mountRef.current.appendChild(renderer.domElement);

    const textureLoader = new THREE.TextureLoader();
    textureLoader.load(
      process.env.PUBLIC_URL + "/images/globe.jpeg",
      function (texture) {
        const sphere = new THREE.Mesh(
          new THREE.SphereGeometry(15, 50, 50),
          new THREE.MeshStandardMaterial({ map: texture })
        );
        sphere.castShadow = true;
        scene.add(sphere);

        const light = new THREE.DirectionalLight(0xffffff, 1);
        light.position.set(50, 50, 50);
        light.castShadow = true;
        scene.add(light);
        scene.add(new THREE.AmbientLight(0x404040));

        camera.position.z = 60;

        const animate = () => {
          requestAnimationFrame(animate);
          sphere.rotation.y += 0.002;
          renderer.render(scene, camera);
        };
        animate();
      },
      undefined,
      function (error) {
        console.error("An error occurred while loading the texture: ", error);
      }
    );

    const handleResize = () => {
      renderer.setSize(window.innerWidth, window.innerHeight);
      camera.aspect = window.innerWidth / window.innerHeight;
      camera.updateProjectionMatrix();
    };

    window.addEventListener("resize", handleResize);

    return () => {
      if (renderer.domElement.parentElement) {
        renderer.domElement.parentElement.removeChild(renderer.domElement);
      }
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return <div ref={mountRef} className="globe" />;
}

export default App;
