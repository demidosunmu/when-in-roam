import { useCallback, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useAddEventMutation } from "./app/api";
import { preventDefault } from "./app/utils";
import {
  updateField,
  clearForm,
  EVENT_FORM,
  showNotification,
  hideNotification,
  updateAutocomplete,
} from "./app/eventSlice";
import Notification from "./Notification";
import { useNavigate } from "react-router-dom";
import { Autocomplete } from "@react-google-maps/api";
import "./event-F.css";

function EventForm() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const {
    show,
    name,
    start_date,
    end_date,
    address,
    latitude,
    longitude,
    description,
    minimum_attendees,
    maximum_attendees,
    fee,
    minimum_age,
    requirements,
    pet_friendly,
    notification,
  } = useSelector((state) => state.event);
  // const formClass = `form ${show === EVENT_FORM ? "is-active" : ""}`;
  const [addEvent, { isLoading: addEventLoading }] = useAddEventMutation();
  const field = useCallback(
    (e) =>
      dispatch(updateField({ field: e.target.name, value: e.target.value })),
    [dispatch]
  );

  const addressRef = useRef(null);
  const today = new Date();
  const currentDate = today.toISOString().split("T")[0];

  const handlePlaceChanged = () => {
    if (addressRef.current) {
      const place = addressRef.current.getPlace();

      if (place && place.geometry && place.geometry.location) {
        const lat = place.geometry.location.lat();
        const lon = place.geometry.location.lng();
        const formattedAddress = place.formatted_address || "";
        dispatch(
          updateAutocomplete({
            placeId: place.place_id,
            formattedAddress: formattedAddress,
          })
        );
        dispatch(updateField({ field: "address", value: formattedAddress }));
        dispatch(updateField({ field: "latitude", value: lat }));
        dispatch(updateField({ field: "longitude", value: lon }));
      }
    }
  };

  const handleSubmit = async () => {
    const { data } = await addEvent({
      name,
      start_date,
      end_date,
      address,
      latitude,
      longitude,
      description,
      minimum_attendees,
      maximum_attendees,
      fee,
      minimum_age,
      requirements,
      pet_friendly,
    });

    if (data) {
      dispatch(
        showNotification({
          type: "success",
          message: "Event created successfully!",
        })
      );

      setTimeout(() => {
        dispatch(hideNotification());
        dispatch(clearForm());
      }, 2000);
    } else {
      dispatch(
        showNotification({
          type: "danger",
          message: "Event not created",
        })
      );
    }
  };

  return (
    <div
      className={`form ${
        show === EVENT_FORM ? "is-active" : ""
      } image-background`}
      key="event-form"
    >
      <div className="form-background"></div>
      <div className="form-content">
        <div className="box-content">
          <h3>Create an Event</h3>
          <form method="POST" onSubmit={preventDefault(handleSubmit)}>
            <div className="field">
              <label className="label">Event Name</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={name}
                  name="name"
                  className="input"
                  type="text"
                  placeholder="Event Name"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Start Date/Time</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={start_date}
                  name="start_date"
                  className="input"
                  type="datetime-local"
                  placeholder="MM/DD/YYYY TOO:OO"
                  min={currentDate}
                />
              </div>
            </div>
            <div className="field">
              <label className="label">End Date/Time</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={end_date}
                  name="end_date"
                  className="input"
                  type="datetime-local"
                  placeholder="MM/DD/YYYY TOO:OO"
                  min={currentDate}
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Event Address</label>
              <div className="control">
                <Autocomplete
                  onLoad={(ref) => (addressRef.current = ref)}
                  onPlaceChanged={handlePlaceChanged}
                >
                  <input
                    required
                    onChange={field}
                    value={address}
                    name="address"
                    className="input"
                    type="text"
                    placeholder="Event Address"
                  />
                </Autocomplete>
              </div>
            </div>
            <div className="field">
              <label className="label">Event Description</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={description}
                  name="description"
                  className="input"
                  type="text"
                  placeholder="Event Description"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Minimum Attendees</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={minimum_attendees}
                  name="minimum_attendees"
                  className="input"
                  type="number"
                  placeholder="Minimum Attendees"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Maximum Attendees</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={maximum_attendees}
                  name="maximum_attendees"
                  className="input"
                  type="number"
                  placeholder="Maximum Attendees"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Fee</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={fee}
                  name="fee"
                  className="input"
                  type="number"
                  placeholder="Fee"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Minimum Age</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={minimum_age}
                  name="minimum_age"
                  className="input"
                  type="number"
                  placeholder="Minimum Age"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Requirements</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={requirements}
                  name="requirements"
                  className="input"
                  type="text"
                  placeholder="Requirements"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Pet Friendly</label>
              <div className="control">
                <input
                  onChange={(e) =>
                    dispatch(
                      updateField({
                        field: e.target.name,
                        value: e.target.checked,
                      })
                    )
                  }
                  checked={pet_friendly}
                  name="pet_friendly"
                  className="input"
                  type="checkbox"
                  placeholder="Pet Friendly"
                />
              </div>
            </div>
            <div className="field-is-grouped">
              <div className="control">
                <button
                  disabled={addEventLoading}
                  className="button is-primary"
                >
                  Submit
                </button>
              </div>
              <div className="control">
                <button
                  type="button"
                  onClick={() => navigate("/events")}
                  className="button"
                >
                  Return to Events
                </button>
              </div>
            </div>
          </form>
          {notification && (
            <Notification type={notification.type}>
              {notification.message}
            </Notification>
          )}
        </div>
      </div>
    </div>
  );
}

export default EventForm;
