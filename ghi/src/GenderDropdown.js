import React from "react";

function GenderDropdown({ value, onChange }) {
  const handleGenderChange = (event) => {
    onChange(event.target.value);
  };

  const genderOptions = [
    { name: "Select your gender", value: "S" },
    { name: "Male", value: "M" },
    { name: "Female", value: "F" },
    { name: "Non-binary", value: "NB" },
    { name: "Agender/I don't identify with any gender", value: "A" },
    { name: "My gender is not listed", value: "O" },
    { name: "Prefer not to state", value: "X" },
  ];

  return (
    <div>
      <label htmlFor="gender">Gender</label>
      <select
        class="custom-select"
        id="gender"
        onChange={handleGenderChange}
        value={value}
      >
        {genderOptions.map((gender) => (
          <option key={gender.value} value={gender.name}>
            {gender.name}
          </option>
        ))}
      </select>
    </div>
  );
}

export default GenderDropdown;
