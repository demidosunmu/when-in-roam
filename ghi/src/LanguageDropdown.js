import React, { useState } from "react";
import { languageOptions } from "./world-languages-simple";

function LanguageDropdown(props) {
  const [selectedLanguages, setSelectedLanguages] = useState([]);

  const handleLanguageChange = (event) => {
    const selectedLanguages = Array.from(
      event.target.selectedOptions,
      (option) => option.value
    );
    setSelectedLanguages(selectedLanguages);
    props.onChange(selectedLanguages);
  };

  return (
    <div>
      <label htmlFor="languages">Select applicable languages</label>
      <select
        id="languages"
        multiple
        onChange={handleLanguageChange}
        value={selectedLanguages}
        className="form-select"
      >
        {languageOptions.map((language) => (
          <option key={language.id} value={language.name}>
            {language.name}
          </option>
        ))}
      </select>
    </div>
  );
}

export default LanguageDropdown;
