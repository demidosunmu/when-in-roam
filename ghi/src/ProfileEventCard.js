import React from "react";
import { Link } from "react-router-dom";
import { formatDateTime } from "./dateUtils";

function ProfileEventCard({ event }) {
  return (
    <div>
      <div>
        <Link
          to={`/events/${event.id}`}
          style={{ textDecoration: "none", color: "inherit" }}
        >
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">{event.event_name}</h5>
              <p className="card-text">
                <strong>Start Date:</strong>{" "}
                {formatDateTime(event.event_start_date)}
              </p>
              <p className="card-text">
                <strong>End Date:</strong>{" "}
                {formatDateTime(event.event_end_date)}
              </p>
              <div className="card-overlay">
                <p className="overlay-text">Show Details</p>
              </div>
            </div>
          </div>
        </Link>
      </div>
    </div>
  );
}

export default ProfileEventCard;
