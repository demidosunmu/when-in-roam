import React, { useState } from "react";
import { useGetEventsQuery } from "./app/api";
import { Link } from "react-router-dom";
import Notification from "./Notification";
import { FaPlusCircle } from "react-icons/fa";
import { formatDateTime } from "./dateUtils";
import "./event-L.css";

function EventList() {
  const { data: events, error, isLoading } = useGetEventsQuery();
  const [filters, setFilters] = useState({
    startDate: "",
    endDate: "",
    fee: "",
    petFriendly: false,
  });

  const clearFilters = () => {
    setFilters({
      startDate: "",
      endDate: "",
      fee: "",
      petFriendly: false,
    });
  };

  const handleFilterChange = (event) => {
    const { name, value, type } = event.target;
    const filterValue = type === "checkbox" ? event.target.checked : value;
    setFilters((prevFilters) => ({
      ...prevFilters,
      [name]: filterValue,
    }));
  };

  const filteredEvents = events?.filter((event) => {
    if (
      filters.startDate &&
      new Date(event.start_date) < new Date(filters.startDate)
    ) {
      return false;
    }
    if (
      filters.endDate &&
      new Date(event.end_date) > new Date(filters.endDate)
    ) {
      return false;
    }
    if (filters.fee && event.fee > parseFloat(filters.fee)) {
      return false;
    }
    if (filters.petFriendly && !event.pet_friendly) {
      return false;
    }
    return true;
  });

  if (isLoading) {
    return <div className="container">Loading...</div>;
  }

  if (error) {
    return (
      <div className="container">
        <Notification type="danger">
          {error.data.detail.map((err, index) => (
            <div key={index}>
              <strong>Error {index + 1}:</strong>
              <div>Location: {err.loc.join(", ")}</div>
              <div>Message: {err.msg}</div>
              <div>Type: {err.type}</div>
            </div>
          ))}
        </Notification>
      </div>
    );
  }

  if (!events) {
    return <div className="container">No events found</div>;
  }

  return (
    <div className="card-container">
      <div className="card-content">
        <h3>Event List</h3>
        <div className="card-header">
          <Link
            to="/events/create"
            style={{
              position: "relative",
              top: "50%",
              left: "50%",
              transform: "translateY(-50%)",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: "40px",
              height: "40px",
              backgroundColor: "#37f564",
              borderRadius: "50%",
              color: "white",
              textDecoration: "none",
              zIndex: 1,
            }}
          >
            <FaPlusCircle size={0} />
          </Link>
        </div>
        <div>
          `
          <label className="filter-label">
            Start Date:
            <input
              type="date"
              name="startDate"
              value={filters.startDate}
              onChange={handleFilterChange}
            />
          </label>
          <label className="filter-label">
            End Date:
            <input
              type="date"
              name="endDate"
              value={filters.endDate}
              onChange={handleFilterChange}
            />
          </label>
          <label className="filter-label">
            Fee:
            <input
              type="number"
              name="fee"
              value={filters.fee}
              onChange={handleFilterChange}
            />
          </label>
          <label className="filter-label">
            Pet Friendly:
            <input
              type="checkbox"
              name="petFriendly"
              checked={filters.petFriendly}
              onChange={handleFilterChange}
            />
          </label>
          <button onClick={clearFilters}>Clear Filters</button>
        </div>
        <div className="row">
          {filteredEvents.map((event) => (
            <div className="col-sm-3">
              <Link
                to={`/events/${event.id}`}
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title">{event.name}</h4>
                    <p className="card-text">
                      Start Date: {formatDateTime(event.start_date)}
                    </p>
                    <p className="card-text">
                      End Date: {formatDateTime(event.end_date)}
                    </p>
                    <p className="card-text">Address: {event.address}</p>
                    <div className="card-overlay">
                      <p className="overlay-text">Show Details</p>
                    </div>
                  </div>
                </div>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default EventList;
