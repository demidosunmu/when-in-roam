import React, { useState, useEffect, useRef } from "react";
import { Link, useNavigate } from "react-router-dom";
import {
  GoogleMap,
  MarkerF,
  InfoWindow,
  DirectionsRenderer,
  StandaloneSearchBox,
} from "@react-google-maps/api";
import { FaPlusCircle } from "react-icons/fa";
import { useSelector, useDispatch } from "react-redux";
import { toggleListView } from "./app/mapSlice";
import { useGetEventsQuery } from "./app/api";
import EventList from "./EventList";
import { formatDateTime } from "./dateUtils";
import Notification from "./Notification";
import "./Eventmap.css";

const Map = () => {
  const containerStyle = {
    position: "relative",
    width: "75%",
    height: "70vh",
    margin: "0 auto",
  };

  const [selectedMarker, setSelectedMarker] = useState(null);
  const [center, setCenter] = useState({});
  const [origin, setOrigin] = useState("");
  const [destination, setDestination] = useState("");
  const [directions, setDirections] = useState(null);
  const [mapCenter, setMapCenter] = useState(center);
  const originSearchBox = useRef(null);
  const destinationSearchBox = useRef(null);
  const navigate = useNavigate();
  const showListView = useSelector((state) => state.map.listView);
  console.log(showListView);
  const dispatch = useDispatch();
  const { data: events, error, isLoading } = useGetEventsQuery();

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const { latitude, longitude } = position.coords;
        const initialCenter = { lat: latitude, lng: longitude };
        setCenter(initialCenter);
      });
    }
  }, []);

  // if (isLoading) return <div>Loading...</div>;
  // if (error) return <div>Error: {error.message}</div>;

  const handleMarkerClick = (event) => {
    setSelectedMarker(event);
  };

  const handleInfoWindowClose = () => {
    setSelectedMarker(null);
  };

  const handleDirectionsResponse = (response, status) => {
    if (status === "OK") {
      setDirections(response);
    } else {
      console.log("Directions request failed:", status);
    }
  };

  const handleCalculateRoute = () => {
    if (origin && destination) {
      const directionsService = new window.google.maps.DirectionsService();
      directionsService.route(
        {
          origin,
          destination,
          travelMode: window.google.maps.TravelMode.DRIVING,
        },
        handleDirectionsResponse
      );
    }
  };

  const handleOriginPlaceSelect = () => {
    const place = originSearchBox.current.getPlaces();
    setOrigin(place[0].formatted_address);
  };

  const handleDestinationPlaceSelect = () => {
    const place = destinationSearchBox.current.getPlaces();
    setDestination(place[0].formatted_address);
  };

  const centerToCurrentLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const { latitude, longitude } = position.coords;
        const updatedCenter = { lat: latitude, lng: longitude };
        setMapCenter(updatedCenter);
      });
    }
  };

  const handleToggleListView = () => {
    dispatch(toggleListView());
  };

  if (isLoading || !events) {
    return <div className='container'>Loading...</div>;
  }

  if (error) {
    return (
      <div className='container'>
        <Notification type='danger'>
          {error.data.detail.map((err, index) => (
            <div key={index}>
              <strong>Error {index + 1}:</strong>
              <div>Location: {err.loc.join(", ")}</div>
              <div>Message: {err.msg}</div>
              <div>Type: {err.type}</div>
            </div>
          ))}
        </Notification>
      </div>
    );
  }

  console.log(events);

  return (
    <>
      {showListView ? (
        <EventList />
      ) : (
        <>
          <GoogleMap
            mapContainerStyle={containerStyle}
            center={Object.keys(center).length === 0 ? mapCenter : center}
            zoom={13}
          >
            <div
              style={{
                position: "absolute",
                display: "flex",
                flexDirection: "column",
                top: "60px",
                left: "110px",
                transform: "translateX(-50%)",
                zIndex: 1,
                backgroundColor: "#ffffff",
                padding: "10px",
                borderRadius: "5px",
              }}
            >
              <div>
                <label>Origin:</label>
                <StandaloneSearchBox
                  onLoad={(ref) => (originSearchBox.current = ref)}
                  onPlacesChanged={handleOriginPlaceSelect}
                >
                  <input
                    value={origin}
                    onChange={(e) => setOrigin(e.target.value)}
                    placeholder='Enter origin'
                  />
                </StandaloneSearchBox>
              </div>
              <div>
                <label>Destination:</label>
                <StandaloneSearchBox
                  onLoad={(ref) => (destinationSearchBox.current = ref)}
                  onPlacesChanged={handleDestinationPlaceSelect}
                >
                  <input
                    value={destination}
                    onChange={(e) => setDestination(e.target.value)}
                    placeholder='Enter destination'
                  />
                </StandaloneSearchBox>
              </div>
              <button onClick={handleCalculateRoute}>Calculate Route</button>
              {directions && (
                <div>
                  <p>Distance: {directions.routes[0].legs[0].distance.text}</p>
                  <p>Duration: {directions.routes[0].legs[0].duration.text}</p>
                </div>
              )}
            </div>
            <button
              onClick={centerToCurrentLocation}
              style={{
                position: "absolute",
                display: "flex",
                top: "230px",
                left: "8px",
                zIndex: 1,
                padding: "5px",
                borderRadius: "5px",
              }}
            >
              Current Location
            </button>
            {selectedMarker && (
              <InfoWindow
                position={{
                  lat: selectedMarker.latitude,
                  lng: selectedMarker.longitude,
                }}
                onCloseClick={handleInfoWindowClose}
              >
                <div>
                  <h5>{selectedMarker.name}</h5>
                  <p>Start Date: {formatDateTime(selectedMarker.start_date)}</p>
                  <p>End Date: {formatDateTime(selectedMarker.end_date)}</p>
                  <p>{selectedMarker.address}</p>
                  <button
                    onClick={() => navigate(`/events/${selectedMarker.id}`)}
                  >
                    View Details
                  </button>
                </div>
              </InfoWindow>
            )}
            {directions && (
              <DirectionsRenderer
                options={{
                  directions: directions,
                }}
              />
            )}
            {events.map((event) => {
              console.log(event.id);
              return (
                <MarkerF
                  key={event.id}
                  position={{ lat: event.latitude, lng: event.longitude }}
                  onClick={() => handleMarkerClick(event)}
                />
              );
            })}
          </GoogleMap>
          <Link
            to='/events/create'
            style={{
              position: "absolute",
              display: "flex",
              top: "480px",
              left: "80px",
              alignItems: "center",
              justifyContent: "center",
              width: "40px",
              height: "40px",
              backgroundColor: "#37f564",
              borderRadius: "50%",
              color: "white",
              zIndex: 1,
            }}
          >
            <FaPlusCircle size={0} />
          </Link>
        </>
      )}
      <button
        onClick={handleToggleListView}
        style={{
          position: "absolute",
          top: "120px",
          left: "211px",
          zIndex: 1,
          backgroundColor: "#ffffff",
          padding: "5px",
          borderRadius: "5px",
        }}
      >
        {showListView ? "Show Map View" : "Show List View"}
      </button>
    </>
  );
};

export default Map;
