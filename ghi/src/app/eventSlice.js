import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  show: null,
  name: "",
  start_date: "",
  end_date: "",
  address: "",
  description: "",
  minimum_attendees: "",
  maximum_attendees: "",
  fee: "",
  minimum_age: "",
  requirements: "",
  pet_friendly: false,
  notification: null,
  rsvps: {},
  autocompletePlaceId: null,
  autocompleteFormattedAddress: "",
};

export const eventSlice = createSlice({
  name: "event",
  initialState,
  reducers: {
    updateField: (state, action) => {
      state[action.payload.field] = action.payload.value;
    },
    showForm: (state, action) => {
      state.show = action.payload;
    },
    clearForm: (state) => {
      return {
        ...state,
        ...initialState,
      };
    },
    showNotification: (state, action) => {
      state.notification = {
        type: action.payload.type,
        message: action.payload.message,
      };
    },
    hideNotification: (state) => {
      state.notification = null;
    },
    updateAutocomplete: (state, action) => {
      const { placeId, formattedAddress } = action.payload;
      state.autocompletePlaceId = placeId;
      state.autocompleteFormattedAddress = formattedAddress;
    },
  },
});

export const {
  clearForm,
  updateField,
  showForm,
  showNotification,
  hideNotification,
  updateAutocomplete,
} = eventSlice.actions;

export const EVENT_FORM = "EVENT_FORM";

export default eventSlice.reducer;
