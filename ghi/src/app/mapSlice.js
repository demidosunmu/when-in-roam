import { createSlice } from "@reduxjs/toolkit";

export const mapSlice = createSlice({
  name: "map",
  initialState: {
    listView: false,
  },
  reducers: {
    toggleListView: (state) => {
      state.listView = !state.listView;
    },
  },
});

export const { toggleListView } = mapSlice.actions;

export default mapSlice.reducer;
