import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  show: null,
  first_name: "",
  last_name: "",
  birthday: "",
  gender: "",
  language: [],
};

export const profileSlice = createSlice({
  name: "profile",
  initialState,
  reducers: {
    setProfile: (state, action) => {
      const { first_name, last_name, birthday, gender, language } =
        action.payload.account;
      state.first_name = first_name;
      state.last_name = last_name;
      state.birthday = birthday;
      state.gender = gender;
      state.language = language;
    },
    updateField: (state, action) => {
      state[action.payload.field] = action.payload.value;
    },
    showModal: (state, action) => {
      state.show = action.payload;
    },
    clearForm: () => {
      return initialState;
    },
  },
});

export const { setProfile, updateField, showModal, clearForm } =
  profileSlice.actions;

export const PROFILE_MODAL = "PROFILE_MODAL";
