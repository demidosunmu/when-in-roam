import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  show: null,
  first_name: "",
  last_name: "",
  birthday: "",
  gender: "",
  language: [],
  email: "",
  password: "",
};

export const accountSlice = createSlice({
  name: "account",
  initialState,
  reducers: {
    updateField: (state, action) => {
      state[action.payload.field] = action.payload.value;
    },
    showModal: (state, action) => {
      state.show = action.payload;
    },
    clearModal: () => {
      return initialState;
    },
  },
});

export const { clearModal, updateField, showModal } = accountSlice.actions;

export const LOG_IN_MODAL = "LOG_IN_MODAL";
export const SIGN_UP_MODAL = "SIGN_UP_MODAL";
export const PROFILE_MODAL = "PROFILE_MODAL";
