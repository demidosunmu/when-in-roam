import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { apiSlice } from "./api";
import { accountSlice } from "./accountSlice";
import { eventSlice } from "./eventSlice";
import { mapSlice } from "./mapSlice";
import { rsvpSlice } from "./rsvpSlice";

export const store = configureStore({
  reducer: {
    [apiSlice.reducerPath]: apiSlice.reducer,
    [accountSlice.name]: accountSlice.reducer,
    [eventSlice.name]: eventSlice.reducer,
    [mapSlice.name]: mapSlice.reducer,
    [rsvpSlice.name]: rsvpSlice.reducer,
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware().concat(apiSlice.middleware);
  },
});

setupListeners(store.dispatch);
