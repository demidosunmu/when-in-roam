import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  hasRSVPed: false,
};

export const rsvpSlice = createSlice({
  name: "rsvp",
  initialState,
  reducers: {
    setHasRSVPed: (state, action) => {
      state.hasRSVPed = action.payload;
    },
  },
});

export const { setHasRSVPed } = rsvpSlice.actions;

export default rsvpSlice.reducer;
