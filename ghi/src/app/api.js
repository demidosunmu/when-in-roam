import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { clearModal } from "./accountSlice";
import { clearForm } from "./eventSlice";

export const apiSlice = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_HOST,
    prepareHeaders: (headers, { getState }) => {
      const selector = apiSlice.endpoints.getToken.select();
      const { data: tokenData } = selector(getState());
      if (tokenData && tokenData.access_token) {
        headers.set("Authorization", `Bearer ${tokenData.access_token}`);
      }
      return headers;
    },
  }),
  tagTypes: ["Account", "Events", "Token"],
  endpoints: (builder) => ({
    signUp: builder.mutation({
      query: (data) => ({
        url: "/accounts",
        method: "post",
        body: data,
        credentials: "include",
      }),
      providesTags: ["Account"],
      invalidatesTags: (result) => {
        return (result && ["Token"]) || [];
      },
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        try {
          await queryFulfilled;
          dispatch(clearModal());
        } catch (err) {}
      },
    }),
    logIn: builder.mutation({
      query: (info) => {
        let formData = null;
        if (info instanceof HTMLElement) {
          formData = new FormData(info);
        } else {
          formData = new FormData();
          formData.append("username", info.email);
          formData.append("password", info.password);
        }
        return {
          url: "/token",
          method: "post",
          body: formData,
          credentials: "include",
        };
      },
      providesTags: ["Account"],
      invalidatesTags: (result) => {
        return (result && ["Token"]) || [];
      },
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        try {
          await queryFulfilled;
          dispatch(clearModal());
        } catch (err) {}
      },
    }),
    updateAccount: builder.mutation({
      query: (email, updates) => {
        console.log("updates:", updates);
        return {
          url: `accounts/${email}`,
          method: "put",
          body: updates,
        };
      },
    }),
    logOut: builder.mutation({
      query: () => ({
        url: "/token",
        method: "delete",
        credentials: "include",
      }),
      invalidatesTags: ["Account", "Token"],
    }),
    getToken: builder.query({
      query: () => ({
        url: "/token",
        credentials: "include",
      }),
      providesTags: ["Token"],
    }),
    addEvent: builder.mutation({
      query: (data) => {
        const feeValue = parseFloat(data.fee).toFixed(2);
        data.fee = feeValue;
        return {
          method: "post",
          url: "/events",
          credentials: "include",
          body: JSON.stringify(data),
          headers: {
            "Content-Type": "application/json",
          },
        };
      },
      invalidatesTags: [{ type: "Events", id: "LIST" }],
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        try {
          await queryFulfilled;
          dispatch(clearForm());
        } catch (err) {}
      },
    }),
    getEvents: builder.query({
      query: () => "/events/",
      providesTags: (data) => {
        const tags = [{ type: "Events", id: "LIST" }];
        if (!data || !data.events) return tags;

        const { events } = data;
        if (events) {
          tags.concat(...events.map(({ id }) => ({ type: "Events", id })));
        }
        console.log(tags);
        return tags;
      },
    }),
    getEvent: builder.query({
      query: (id) => `/events/${id}`,
      providesTags: (data) => {
        const tags = [{ type: "Events", id: "LIST" }];
        if (!data || !data.event) return tags;

        const { event } = data;
        if (event) {
          tags.push({ type: "Events", id: event.id });
        }
        return tags;
      },
    }),
    getRSVPs: builder.query({
      query: (eventId) => `/events/${eventId}/rsvp`,
    }),
    getRSVPByUserId: builder.query({
      query: (userId) => `/rsvps/${userId}`,
    }),
    createRSVP: builder.mutation({
      query: ({ eventId, accountId }) => ({
        url: `events/${eventId}/rsvp`,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: { accountId },
      }),
    }),

    deleteRSVP: builder.mutation({
      query: (rsvpId) => ({
        url: `/events/rsvp/${rsvpId}`,
        method: "DELETE",
      }),
    }),
  }),
});

export const {
  useAddEventMutation,
  useGetEventsQuery,
  useGetEventQuery,
  useGetTokenQuery,
  useUpdateAccountMutation,
  useLogInMutation,
  useLogOutMutation,
  useSignUpMutation,
  useGetRSVPsQuery,
  useGetRSVPByUserIdQuery,
  useCreateRSVPMutation,
  useDeleteRSVPMutation,
} = apiSlice;
