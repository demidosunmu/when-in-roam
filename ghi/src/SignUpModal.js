import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useSignUpMutation } from "./app/api";
import { preventDefault } from "./app/utils";
import { showModal, updateField, SIGN_UP_MODAL } from "./app/accountSlice";
import Notification from "./Notification";
import { useNavigate } from "react-router-dom";
import LanguageDropdown from "./LanguageDropdown";
import GenderDropdown from "./GenderDropdown";

function SignUpModal() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const {
    show,
    first_name,
    last_name,
    birthday,
    gender,
    language,
    email,
    password,
  } = useSelector((state) => state.account);
  const modalClass = `modal ${show === SIGN_UP_MODAL ? "is-active" : ""}`;
  const [signUp, { error, isLoading: signUpLoading }] = useSignUpMutation();
  const field = useCallback(
    (e) =>
      dispatch(updateField({ field: e.target.name, value: e.target.value })),
    [dispatch]
  );

  const handleLanguageChange = (selectedLanguages) => {
    dispatch(updateField({ field: "language", value: selectedLanguages }));
  };

  const handleGenderChange = (selectedGender) => {
    dispatch(updateField({ field: "gender", value: selectedGender }));
  };

  const handleSubmit = async () => {
    try {
      const { data } = await signUp({
        first_name,
        last_name,
        birthday,
        gender,
        language,
        email,
        password,
      });

      if (data) {
        navigate("/events");
      }
    } catch (error) {
      if (error.message.includes("An account with this email already exists")) {
      }
    }
  };

  return (
    <div className={modalClass} key="signup-modal">
      <div className="modal-background"></div>
      <div className="modal-content">
        <div className="box-content">
          <h3>Sign Up</h3>
          {error ? (
            <Notification type="danger">{error.data.detail}</Notification>
          ) : null}
          <form method="POST" onSubmit={preventDefault(handleSubmit)}>
            <div className="field">
              <label className="label">First Name</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={first_name}
                  name="first_name"
                  className="input"
                  type="text"
                  placeholder="Your First Name"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Last Name</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={last_name}
                  name="last_name"
                  className="input"
                  type="text"
                  placeholder="Your Last Name"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Birthday</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={birthday}
                  name="birthday"
                  className="input"
                  type="date"
                  placeholder="MM/DD/YYYY"
                />
              </div>
            </div>
            <div className="field">
              <div className="control">
                <GenderDropdown onChange={handleGenderChange} value={gender} />
              </div>
            </div>
            <div className="field">
              <label className="label">Language</label>
              <div className="control">
                <LanguageDropdown
                  onChange={handleLanguageChange}
                  value={language}
                />
              </div>
            </div>
            <div className="field">
              <label className="label" htmlFor="email">
                Email
              </label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={email}
                  name="email"
                  className="input"
                  type="email"
                  placeholder="you@example.com"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Password</label>
              <div className="control">
                <input
                  required
                  onChange={field}
                  value={password}
                  name="password"
                  className="input"
                  type="password"
                  placeholder="secret..."
                />
              </div>
            </div>
            <div className="field-is-grouped">
              <div className="control">
                <button disabled={signUpLoading} className="button is-primary">
                  Submit
                </button>
              </div>
              <div className="control">
                <button
                  type="button"
                  onClick={() => dispatch(showModal(null))}
                  className="button"
                >
                  Cancel
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SignUpModal;
