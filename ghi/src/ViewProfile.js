import React from "react";
import { useGetTokenQuery, useGetRSVPByUserIdQuery } from "./app/api";
import { Link } from "react-router-dom";
import ProfileEventCard from "./ProfileEventCard";

function ViewProfile() {
  const {
    data: userData,
    isLoading: userLoading,
    error: userError,
  } = useGetTokenQuery();

  const accountId = userData?.account?.id || null;

  const {
    data: rsvpData,
    isLoading: rsvpLoading,
    error: rsvpError,
  } = useGetRSVPByUserIdQuery(accountId, {
    enabled: !!accountId,
    refetchOnMountOrArgChange: true,
  });

  if (userLoading || rsvpLoading) {
    return <div>Loading...</div>;
  }

  if (userError || rsvpError) {
    return (
      <div>Error: {userError ? userError.message : rsvpError.message}</div>
    );
  }

  const birthday = new Date(userData.account.birthday);
  const today = new Date();
  const age = today.getFullYear() - birthday.getFullYear();
  const languages = userData.account.language.join(", ");

  const upcomingRSVPs = rsvpData.filter((rsvp) => {
    const eventDate = new Date(rsvp.event_end_date);
    return eventDate >= today;
  });

  const previousRSVPs = rsvpData.filter((rsvp) => {
    const eventDate = new Date(rsvp.event_end_date);
    return eventDate < today;
  });

  return (
    <main className="profile-container">
      <h1>
        {userData.account.first_name} {userData.account.last_name}
      </h1>
      <p>
        {userData.account.email} - {age} years old - {userData.account.gender} -{" "}
        {languages}
      </p>

      <div>
        <h2>Upcoming Events</h2>
        <div className="profile-row">
          {upcomingRSVPs.length > 0 ? (
            upcomingRSVPs.map((rsvp) => (
              <div className="box-content" key={rsvp.id}>
                <ProfileEventCard event={rsvp} />
              </div>
            ))
          ) : (
            <p>You have no upcoming events.</p>
          )}
        </div>
      </div>

      <div>
        <h2>Previous Events</h2>
        <div className="profile-row">
          {previousRSVPs.length > 0 ? (
            previousRSVPs.map((rsvp) => (
              <div className="box-content" key={rsvp.id}>
                <ProfileEventCard event={rsvp} />
              </div>
            ))
          ) : (
            <p>You have no previous events.</p>
          )}
        </div>
        <Link
          to="/events/create"
          style={{ textDecoration: "none", color: "inherit" }}
        >
          <button>Create Event</button>
        </Link>
      </div>
    </main>
  );
}

export default ViewProfile;
